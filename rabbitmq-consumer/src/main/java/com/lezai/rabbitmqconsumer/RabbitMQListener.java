package com.lezai.rabbitmqconsumer;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class RabbitMQListener {
    @RabbitListener(queues = "queue_test_simple_mode")
    public void onMessage(Message message, Channel channel) throws Exception {
        System.out.println("收到消息：" + new String(message.getBody()));
    }
    @RabbitListener(queues = "queue_test_fanout_mode_1")
    public void onMessage1(Message message, Channel channel) throws Exception {
        System.out.println("收到消息来自队列queue_test_fanout_mode_1：" + new String(message.getBody()));
    }
    @RabbitListener(queues = "queue_test_fanout_mode_2")
    public void onMessage2(Message message, Channel channel) throws Exception {
        System.out.println("收到消息来自队列queue_test_fanout_mode_2：" + new String(message.getBody()));
    }
    @RabbitListener(queues = "queue_test_routing_mode_1")
    public void onMessage3(Message message, Channel channel) throws Exception {
        System.out.println("收到消息来自队列queue_test_routing_mode_1：" + new String(message.getBody()));
    }
    @RabbitListener(queues = "queue_test_routing_mode_2")
    public void onMessage4(Message message, Channel channel) throws Exception {
        System.out.println("收到消息来自队列queue_test_routing_mode_2：" + new String(message.getBody()));
    }
    @RabbitListener(queues = "queue_test_dlx")
    public void onMessage5(Message message, Channel channel) throws Exception {
        System.out.println("我收到消息的时间为："+(System.currentTimeMillis()));
        System.out.println("收到消息来自队列queue_test_dlx：" + new String(message.getBody()));
    }
    @RabbitListener(queues = "queue_test_delay_plugin")
    public void onMessage6(Message message, Channel channel) throws Exception {
        System.out.println("我收到消息的时间为："+(System.currentTimeMillis()));
        System.out.println("收到消息来自队列queue_test_delay_plugin：" + new String(message.getBody()));
    }
}
