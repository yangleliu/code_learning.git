package com.lezai.zookeeper.lock;

import org.I0Itec.zkclient.ZkClient;

public abstract class ZkAbstractLock implements lock{
    //zk连接地址
    private static final String CONNECTSTRING = "127.0.0.1:2181";
    //创建zk连接
    protected ZkClient zkClient = new ZkClient(CONNECTSTRING);
    //PATH
    protected static final String PATH = "/lock";
    public abstract boolean tryLock();
    public abstract void waitLock();
    public boolean lock() {
        try{
            if (tryLock()) {
//            System.out.println("###成功获取锁###");
            } else {
                waitLock();
                lock();
            }
        }catch (Exception e){
            return false;
        }
        return true;

    }

    public void unLock() {
        if(zkClient!=null){
            zkClient.close();
//            System.out.println("###释放所资源###");
        }
    }
}
