package com.lezai.rabbitmqproduct;

import com.lezai.rabbitmqproduct.config.RabbitMQConfig;
import net.minidev.json.JSONUtil;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RabbitmqProductApplicationTests {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testDelayPlugin() {
        rabbitTemplate.convertAndSend("exchange_test_delay_plugin", "testDelayPlugin", "测试延时插件发送消息", new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws AmqpException {
                message.getMessageProperties().setDelay(10000);
                return message;
            }
        });
    }

    @Test
    public void testSimpleMode() {
        rabbitTemplate.convertAndSend("", RabbitMQConfig.QUEUE_TEST_SIMPLE_MODE, "测试简单模式");
    }

    @Test
    public void testFanoutMode() {
        rabbitTemplate.convertAndSend("exchange_test_fanout_mode","",  "测试发布订阅模式");
    }
    @Test
    public void testRoutingMode() {
        rabbitTemplate.convertAndSend("exchange_test_routing_mode","order",  "测试路由模式");
    }

//    @Test
//    public void testDLX() {
//        rabbitTemplate.convertAndSend(null, "queue_test_normal","我是10秒之后才到的", new MessagePostProcessor() {
//            @Override
//            public Message postProcessMessage(Message message) throws AmqpException {
//                MessageProperties messageProperties = message.getMessageProperties();
//                messageProperties.setExpiration(10000+"");
//                return message;
//            }
//        });
//        System.out.println("我发送消息的时间为:"+(System.currentTimeMillis()));
//        System.out.println("开始倒计时:10");
//        int i = 10;
//        while (true){
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            if(i>0){
//                System.out.println("倒计时："+(--i));
//            }
//
//        }
//    }


//    @Test
//    void contextLoads() {
//        rabbitTemplate.convertAndSend(RabbitMQConfig.ITEM_TOPIC_EXCHANGE,"item.1111","asdasddas");
//    }
//    @Test
//    void testDefaultMesg() throws InterruptedException {
//        for (int i=0;i<10;i++){
//            rabbitTemplate.convertAndSend("item_queue","测试默认队列");
//            Thread.sleep(2000);
//        }
//    }
    @Test
    void testFanout() {
        rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE_TEST_FANOUT_MODE,"","asdasddas");
    }
//    @Test
//    void testCallback() throws InterruptedException {
//        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
//            @Override
//            public void confirm(CorrelationData correlationData, boolean b, String s) {
//                System.out.println("消息回调"+b+":::"+s);
//            }
//        });
//        rabbitTemplate.setReturnsCallback(new RabbitTemplate.ReturnsCallback() {
//            @Override
//            public void returnedMessage(ReturnedMessage returnedMessage) {
//                System.out.println("setReturnsCallback::"+returnedMessage.getReplyText());
//            }
//        });
//        for (int i=0;i<10;i++){
//            rabbitTemplate.convertAndSend(RabbitMQConfig.ITEM_DIRECT_EXCHANGE,"info","asdasddas");
//            Thread.sleep(2000);
//        }
////        while (true){}
//    }


}
