package com.lezai.zookeeper.lock;

public class TestLock {
    public static void main(String[] args) {

        int i = 0;
        while (i < 50) {
            People people = new People(i);
            people.start();
//            try {
////                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            i++;
//            System.out.println(i);
        }
        System.out.println(People.totalMoney);
    }
    static class People extends Thread{
        private int i = 0;
        public People(int i){
            this.i = i;
        }
        private ZkListenerLock lock = new ZkListenerLock("order");
//        private ZKExceptionLock lock2 = new ZKExceptionLock("order2");
        public static int totalMoney = 50;
        @Override
        public void run(){
            try {
                //获取锁资源
                lock.lock();
                System.out.println("获取锁成功"+i);
                totalMoney--;
//                System.out.println(totalMoney);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                //释放锁资源
                lock.unLock();
                System.out.println("释放锁成功"+i);
            }
        }
    }
}
