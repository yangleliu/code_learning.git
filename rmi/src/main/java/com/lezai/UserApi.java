package com.lezai;

import java.io.Serializable;
import java.rmi.Remote;

public interface UserApi extends Remote , Serializable {
    void out(String name);
}
