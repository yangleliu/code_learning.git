package com.lezai.rabbitmqproduct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitmqProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitmqProductApplication.class, args);
    }

}
