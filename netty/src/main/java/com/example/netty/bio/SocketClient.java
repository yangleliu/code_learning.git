    package com.example.netty.bio;

    import java.io.IOException;
    import java.net.Socket;

    public class SocketClient {
        public static void main(String[] args) throws IOException {
            Socket socket = new Socket("127.0.0.1",9000);
            socket.getOutputStream().write("我是客户端".getBytes());
            socket.getOutputStream().flush();
            System.out.println("向服务端发送数据结束");
            byte[] bytes = new byte[1024];
            try {
                int read = socket.getInputStream().read(bytes);
                System.out.println("服务端发送过来的数据为："+new String(bytes,0,read));
            } catch (IOException e) {
                e.printStackTrace();
            }finally {

            }

        }
    }
