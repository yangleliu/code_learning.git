package com.lezai;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class Consumer {
    public static void main(String[] args) throws RemoteException, NotBoundException, MalformedURLException {
        UserApi userApi = (UserApi) Naming.lookup("rmi://localhost:9999/userApi");
        System.out.println(userApi.toString());
        userApi.out("下班");
    }
}
