package com.lezai.zookeeper.lock;

public class TestReadAndWriteLock {
    public static void main(String[] args) {

        int i = 0;
        while (i < 3) {
            People people = new People(i);
            people.start();
            i++;
//            System.out.println(i);
        }
        System.out.println(People.totalMoney);
    }
    static class People extends Thread{
        private int i = 0;
        public People(int i){
            this.i = i;
        }
        private ZkWriteLock lock = new ZkWriteLock("rw");
        private ZkReadLock lock2 = new ZkReadLock("rw");
//        private ZKExceptionLock lock2 = new ZKExceptionLock("order2");
        public static int totalMoney = 50;
        @Override
        public void run(){
            try {
                //获取锁资源
                lock.lock();
                System.out.println("获取写锁成功"+i);
                //获取锁资源
                lock2.lock();
                System.out.println("获取读锁成功"+i);
                Thread.sleep(2000);
                totalMoney--;
//                System.out.println(totalMoney);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                //释放锁资源
                lock.unLock();
                System.out.println("释放写锁成功"+i);
                lock2.unLock();
                System.out.println("释放读锁成功"+i);
            }
        }
    }
}
