package com.lezai.zookeeper.lock;

import org.I0Itec.zkclient.IZkDataListener;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class ZkListenerLock extends ZkAbstractLock {
    private String lockPath;
    private String currentPath;
    private String beforePath;

    public ZkListenerLock(String lockPath) {
        if (!zkClient.exists(PATH)) {
            zkClient.createPersistent(PATH);
        }
        this.lockPath = PATH + "/" + lockPath;
        if (!zkClient.exists(this.lockPath)) {
            System.out.println("-----------");
            zkClient.createPersistent(this.lockPath);
        }
    }

    public boolean tryLock() {
        if (null == currentPath || currentPath.length() <= 0) {
            currentPath = zkClient.createEphemeralSequential(lockPath + "/", "lock");
        }
        List<String> children = zkClient.getChildren(lockPath);
        Collections.sort(children);
        if (currentPath.equals(lockPath + "/" + children.get(0))) {
            System.out.println("成功获取锁：" + currentPath);
            return true;
        }
        String key = currentPath.substring(lockPath.length() + 1);
        int location = Collections.binarySearch(children, key);
        this.beforePath = lockPath + "/" + children.get(location - 1);
        return false;
    }

    public void waitLock() {
        CountDownLatch LATCH = new CountDownLatch(1);
        IZkDataListener listener = new IZkDataListener() {
            public void handleDataChange(String s, Object o) {
            }

            public void handleDataDeleted(String s) {
                LATCH.countDown();
            }
        };
        zkClient.subscribeDataChanges(this.beforePath, listener);
        if (!zkClient.exists(this.beforePath)) {
            return;
        }
        try {
            LATCH.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        zkClient.unsubscribeDataChanges(this.beforePath, listener);
    }

    @Override
    public void unLock() {
        System.out.println("成功释放锁"+currentPath);
        zkClient.delete(currentPath);
        super.unLock();
    }
}