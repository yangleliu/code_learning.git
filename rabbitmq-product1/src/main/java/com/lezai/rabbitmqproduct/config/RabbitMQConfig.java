package com.lezai.rabbitmqproduct.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RabbitMQConfig {
    public static final String QUEUE_TEST_SIMPLE_MODE = "queue_test_simple_mode";
    //交换机名称
    public static final String ITEM_TOPIC_EXCHANGE = "item_topic_exchange";
    //交换机名称  发布订阅
    public static final String ITEM_FANOUT_EXCHANGE = "item_fanout_exchange";
    public static final String ITEM_DIRECT_EXCHANGE = "item_direct_exchange";
    //队列名称
    public static final String ITEM_QUEUE = "item_queue";

    public static final String QUEUE_TEST_FANOUT_MODE_1 = "queue_test_fanout_mode_1";
    public static final String QUEUE_TEST_FANOUT_MODE_2 = "queue_test_fanout_mode_2";
    public static final String EXCHANGE_TEST_FANOUT_MODE = "exchange_test_fanout_mode";


    public static final String QUEUE_TEST_ROUTING_MODE_1 = "queue_test_routing_mode_1";
    public static final String QUEUE_TEST_ROUTING_MODE_2 = "queue_test_routing_mode_2";
    public static final String EXCHANGE_TEST_ROUTING_MODE = "exchange_test_routing_mode";

    public static final String QUEUE_TEST_DELAY_PLUGIN = "queue_test_delay_plugin";
    public static final String EXCHANGE_TEST_DELAY_PLUGIN = "exchange_test_delay_plugin";


    public static final String QUEUE_TEST_DLX = "queue_test_dlx";
    public static final String QUEUE_TEST_NORMAL = "queue_test_normal";
    public static final String EXCHANGE_TEST_DLX = "exchange_test_dlx";
    //    声明一个队列
    @Bean("queueDelayPlugin")
    public Queue queueDelayPlugin() {
        return QueueBuilder.durable(QUEUE_TEST_DELAY_PLUGIN).build();
    }
//    @Bean
//    CustomExchange delayExchange(){
//        Map<String, Object> args = new HashMap<>();
//        args.put("x-delayed-type", "direct");
//        return new CustomExchange(EXCHANGE_TEST_DELAY_PLUGIN, "x-delayed-message", true,false, args);
//    }

    //    插件交换机与队列绑定
//    @Bean
//    public Binding itemQueueExchange8(@Qualifier("queueDelayPlugin") Queue queue,
//                                      @Qualifier("delayExchange") Exchange exchange) {
//        return BindingBuilder.bind(queue).to(exchange).with("testDelayPlugin").noargs();
//    }



    //    声明一个默认不进行消费的队列
    @Bean("queueTestNormal")
    public Queue queueTestNormal() {
        return QueueBuilder.durable(QUEUE_TEST_NORMAL).deadLetterExchange(EXCHANGE_TEST_DLX).deadLetterRoutingKey("testdlx").build();
    }

    //    声明死信队列
    @Bean("queueTestDLX")
    public Queue queueTestDLX() {
        return QueueBuilder.durable(QUEUE_TEST_DLX).build();
    }
    //  声明死信交换机
    @Bean("exchangeTestDLX")
    public Exchange exchangeTestDLX() {
        return ExchangeBuilder.directExchange(EXCHANGE_TEST_DLX).durable(true).build();
    }

//    死信队列与死信交换机绑定
    @Bean
    public Binding itemQueueExchange7(@Qualifier("queueTestDLX") Queue queue,
                                      @Qualifier("exchangeTestDLX") Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("testdlx").noargs();
    }


    //    声明队列
    @Bean("queueTestSimpleMode")
    public Queue queueTestSimpleMode() {
        return QueueBuilder.durable(QUEUE_TEST_SIMPLE_MODE).build();
    }

    //    声明队列
    @Bean("queueTestFANOUTMode1")
    public Queue queueTestFANOUTMode1() {
        return QueueBuilder.durable(QUEUE_TEST_FANOUT_MODE_1).build();
    }

    //    声明队列
    @Bean("queueTestFANOUTMode2")
    public Queue queueTestFANOUTMode2() {
        return QueueBuilder.durable(QUEUE_TEST_FANOUT_MODE_2).build();
    }

    //  声明交换机
    @Bean("exchangeTestFANOUTMode")
    public Exchange exchangeTestFANOUTMode() {
        return ExchangeBuilder.fanoutExchange(EXCHANGE_TEST_FANOUT_MODE).durable(true).build();
    }

    // 绑定交换机与队列 订阅模式下 routingkey为空
    @Bean
    public Binding itemQueueExchange1(@Qualifier("queueTestFANOUTMode1") Queue queue,
                                      @Qualifier("exchangeTestFANOUTMode") Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("").noargs();
    }

    @Bean
    public Binding itemQueueExchange2(@Qualifier("queueTestFANOUTMode2") Queue queue,
                                      @Qualifier("exchangeTestFANOUTMode") Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("").noargs();
    }

    //    声明队列
    @Bean("queueTestROUTINGMode1")
    public Queue queueTestROUTINGMode1() {
        return QueueBuilder.durable(QUEUE_TEST_ROUTING_MODE_1).build();
    }

    //    声明队列
    @Bean("queueTestROUTINGMode2")
    public Queue queueTestROUTINGMode2() {
        return QueueBuilder.durable(QUEUE_TEST_ROUTING_MODE_2).build();
    }

    //  声明交换机
    @Bean("exchangeTestROUTINGMode")
    public Exchange exchangeTestROUTINGMode() {
        return ExchangeBuilder.directExchange(EXCHANGE_TEST_ROUTING_MODE).durable(true).build();
    }

    // 绑定交换机与队列
    @Bean
    public Binding itemQueueExchange3(@Qualifier("queueTestROUTINGMode1") Queue queue,
                                      @Qualifier("exchangeTestROUTINGMode") Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("order").noargs();
    }

    @Bean
    public Binding itemQueueExchange4(@Qualifier("queueTestROUTINGMode2") Queue queue,
                                      @Qualifier("exchangeTestROUTINGMode") Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("member").noargs();
    }
}
