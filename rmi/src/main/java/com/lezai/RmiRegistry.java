package com.lezai;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class RmiRegistry {
    public static void main(String[] args) throws IOException, AlreadyBoundException {
        LocateRegistry.createRegistry(9999);
        Naming.bind("rmi://localhost:9999/userApi", new UserApi() {
            @Override
            public void out(String name) {
                System.out.println("你好，下班了么:"+name);
            }
        });
        System.out.println("服务启动");
        System.in.read();
    }
}
